FROM centos:7
FROM python:2.7
FROM java:openjdk-8-jdk

##Some properties for proxy
###If you need
ENV  HTTP_PROXY: $YOUR_VARIABLE HTTPS_PROXY: $YOUR_VARIABLE NO_PROXY: $YOUR_VARIABLE

COPY /src/entry_point.sh /opt/bin/entry_point.sh
COPY /src/server.py /opt/bin/server.py
COPY /src/server_index.html /opt/bin/server_index.html
COPY /src/YOU_LIC_FILE.key /opt/YOU_LIC_FILE.key
COPY /src/ready-api-license-manager-1.2.5.jar  /opt/ready-api-license-manager-1.2.5.jar
COPY /src/jre.zip /opt/jre.zip

RUN mkdir -p projects

## PROXY CONFIG FOR WGET & APT
### IF YOU NEED
COPY /src/wgetrc /etc/wgetrc
COPY /src/20.proxy /etc/apt/apt.conf.d/proxy.conf
## READYAPI SOURCE
COPY /src/ReadyAPI-2.5.0-linux-bin.tar.gz /opt/ReadyAPI-2.5.0-linux-bin.tar.gz


# Download and unarchive SoapUI
RUN mkdir -p /opt &&\
	mkdir -p /opt/ReadyAPI
RUN tar -xzf /opt/ReadyAPI-2.5.0-linux-bin.tar.gz --directory /opt && \
    mv /opt/ReadyAPI-2.5.0/* /opt/ReadyAPI/ && \
    rm /opt/ReadyAPI-2.5.0-linux-bin.tar.gz

## SOME MODULES
RUN unzip /opt/jre.zip -d /opt/ReadyAPI/

RUN chmod +x /opt/bin/entry_point.sh
RUN chmod +x /opt/bin/server.py


RUN chmod -R 755 /opt/ReadyAPI/bin

# Add GraphQl libraries
ADD http://central.maven.org/maven2/org/antlr/antlr4/4.5.1/antlr4-4.5.1.jar /opt/ReadyAPI/bin/ext
ADD http://central.maven.org/maven2/com/graphql-java/graphql-java/7.0/graphql-java-7.0.jar /opt/ReadyAPI/bin/ext

# Add PSQL libraries
ADD https://jdbc.postgresql.org/download/postgresql-42.2.5.jar /opt/ReadyAPI/bin/ext

# Activate License
RUN java -jar /opt/ready-api-license-manager-1.2.5.jar -f /opt/YOU_LIC_FILE.key | echo "1"

# System Fonts
RUN apt-get update &&\
    apt-get install -y cabextract\
                       xfonts-utils\
                       fonts-liberation
    
RUN wget http://ftp.br.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.6_all.deb &&\
    dpkg -i ttf-mscorefonts-installer_3.6_all.deb

## Clear proxy settings
RUN echo "" > /etc/wgetrc

# Set working directory
WORKDIR /opt/bin

# Set environment
ENV PATH ${PATH}:/opt/ReadyAPI/bin

EXPOSE 3000
CMD ["/opt/bin/entry_point.sh"]